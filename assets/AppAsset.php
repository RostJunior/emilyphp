<?php
/**
 * 
 * File for connection resources: css, javascript.
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Rost
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/style.css',
        'css/ekko-lightbox.css',
        'css/flexslider.css',
        'css/animate.min.css',
        'css/font-awesome.min.css',
        'css/bootstrap-theme.min.css',
    ];
    public $js = [
        
       
        'js/bootstrap.js',
        'js/jquery.sticky.js',
        'js/jquery.nav.js',
        'js/jquery.scrollTo.js',
        'js/jquery.flexslider.js',
        'js/ekko-lightbox.js',
        'js/jquery.easing.1.3.js',
        'js/jquery.shuffle.js',
        'js/script.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
      //  'yii\bootstrap\BootstrapAsset',
    ];
}
