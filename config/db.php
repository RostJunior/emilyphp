<?php
/**
 * Configure the DB connection
 */
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=emily_php',
    'username' => 'root',
    'password' => '',
    'charset' => 'utf8',
   
];
