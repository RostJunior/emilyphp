<?php

return [
    'adminEmail' => 'ros.tan0601@gmail.com',
    'refCargo' => 'Cargo', //вид груза вантаж
    'typesOfPayers' => 'Recipient', //вид плательщика получатель
    'packList' => 'c5d49e74-4ea1-11e2-889f-001631fa0467', //вид упаковки - Пакування в стрейч плівку (0.1-2 кг)
    'serviceTypes' => 'WarehouseWarehouse', //склад-склад
    'typesOfCounterparties' => 'PrivatePerson', //приватна персона
    'getPaymentForms' => 'Cash', //Наличный расчет
    'watermark' => realpath(dirname(__DIR__)).'\web\images\wtt.png',
    'noPhoto' => 'no-photo.png',
    'pathImg' => realpath(dirname(__DIR__)).'\web\images\portfolio\\',
    'idvk' => '',
    'keyvk' => '',
    'redirect_uri' => 'http://emily.hol.es/site/getauth',
    
];
