<?php

/*
 *общий контроллер
 */

namespace app\controllers;
use yii\web\Controller;

/**
 * Description of AppController
 * 
 * @param string $title to assign the page name 
 * @param string $keywords to assign keywords
 * @param string $description to assign description
 *
 * @author Rost
 */
class AppController extends Controller{
    
    protected function setMeta($title = null, $keywords = null, $description =
            null) {
        
        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
        
            }
}



