<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * 
 */

namespace app\controllers;
use app\controllers\AppController;
use app\models\Product;
use app\models\Cart;
use Yii;
use app\models\Order;
use app\models\OrderItems;
use \app\models\Cities;
use \app\models\EmilyOfficeNP;
use \app\models\DostavkaNP;
use \app\models\InternetDocument;
/**
 * Class CartController to control the behavior of the basket of the website and order confirmation
 *
 * @author Rost
 * 
 * 
 * Array 
 * (
 * [1] => Array (
 *      [qty] => QTY,
 *      [name] => Name,
 *      [price] => Price,
 *      [img] => Img
 * )
 * [10] => Array (
 *      [qty] => QTY,
 *      [name] => Name,
 *      [price] => Price,
 *      [img] => Img
 * )
 * [qty] => QTY,
 * [sum] => Sum
 * )
 */
class CartController extends AppController{
    
     /**
     * Add action of adding a product to cart.
     * @var int $id the product ID obtained from get request
     * @var int $qty the number of products obtained from get request
     * @var array $product the product from the database
     * @return cart-modal
     * @see Cart
     */
    public function actionAdd() {
        $id = Yii::$app->request->get('id');
        $qty = (int) Yii::$app->request->get('qty');
        $qty = !$qty ? 1 : $qty;
        $product = Product::findOne($id);
        if(empty($product)) return false;
        
        $session = Yii::$app->session;
        $session->open();
        
        $cart = new Cart();
        
      $cart->addToCart($product, $qty);
       if(!Yii::$app->request->isAjax) {
           return $this->redirect(Yii::$app->request->referrer);
       }
      $this->layout = FALSE;
      return $this->render('cart-modal', compact('session'));
    }
    
    /**
     * Action to empty the Cart
     * @return cart-modal
     */
    public function actionClear() {
            $session = Yii::$app->session;
            $session->open();
            $session->remove('cart');
             $session->remove('cart.qty');
              $session->remove('cart.sum');
              $this->layout = FALSE;
      return $this->render('cart-modal', compact('session'));
    }
    /**
     * Action to delete an item (product) in the cart
     * @return cart-modal
     */
    public function actionDelItem() {
        $id = Yii::$app->request->get('id');
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->recalc($id);
        $this->layout = FALSE;
        return $this->render('cart-modal', compact('session'));
    }
     /**
     * Action to delete an item (product) in the shopping cart on the order page
     * @return $this->redirect(Yii::$app->request->referrer)
     */
      public function actionDelItemOrder() {
        $id = Yii::$app->request->get('id');
        $session = Yii::$app->session;
        $session->open();
        $cart = new Cart();
        $cart->recalc($id);
        $this->layout = FALSE;
        
           return $this->redirect(Yii::$app->request->referrer);
      
    }
    
    /**
     * Action show Cart 
     * @return cart-modal
     */
    public function actionShow() {
        $session = Yii::$app->session;
        $session->open();
        $this->layout = FALSE;
        return $this->render('cart-modal', compact('session'));
    }
    /**
     * Action of page output order
     * @see page order view
     */
    public function actionView() {
     
       $session = Yii::$app->session;
        $session->open();
        
        $this->setMeta('EMILY | Кошик');
        
        $order = new Order();
        $dostavka = new DostavkaNP();
        $cities = new Cities();
        $office = new EmilyOfficeNP();
        
        if($order->load(Yii::$app->request->post())) {
            $order->qty = $session['cart.qty'];
            $order->sum = $session['cart.sum'];
            $order->dostavka = "samovivoz";
            $order->addressUser = "no";
            if($order->save()) {
                $this->saveOrderItems($session['cart'], $order->id);
                Yii::$app->session->setFlash('success_order', 'Ваше замовлення прийняте!');
                
             //send an email with the order | cartOrder the view the content of the email
      /*          Yii::$app->mailer->compose('cartOrder', compact('session', 'order'))
                        ->setFrom('ros.tan0601@gmail.com')
                        ->setTo($order->emailUser)
                        ->setSubject('Замовлення № '. $order->id)
                        ->send();
       * 
       */
                //remove the cart from the session
                $session->remove('cart');
                $session->remove('cart.qty');
                $session->remove('cart.sum');
                
                return $this->refresh();
                }
            else {
                Yii::$app->session->setFlash('error_order', 'Замовлення не може бути прийняте! (помилка при оформленні) ');
            }
         
         
        }
        return $this->render('view', compact('session', 'order', 'cities', 'office', 'dostavka'));
    }
    
     public function actionViewnp() {
       
        $cityID = Yii::$app->request->get('id');
        $city = Cities::getCityID($cityID);
        $ref = $city->ref;
        $office = EmilyOfficeNP::find()->asArray()->where(['cityRef' => $ref])->all();
        
      
        $this->layout = FALSE;
        return $this->render('select', compact('office'));
        
     }
     
     /**
      * Save order if you choose the delivery
      * @see page order view
      */
     
     public function  actionOrdernp() {
         
        
            $session = Yii::$app->session;
            $session->open();
            $order = new Order();
            $cities = new Cities();
            $office = new EmilyOfficeNP();
            $dostavka = new DostavkaNP();
            $ar = Yii::$app->request->post();
            if($order->load($ar) && $cities->load($ar) 
                    && $office->load($ar) && $dostavka->load($ar)) 
                {
                 $order->qty = $session['cart.qty'];
                $order->sum = $session['cart.sum'];
                $order->dostavka = "NovaPoshta";
                $order->nameUser = $dostavka->contactUser;
                $order->phoneUser = $dostavka->phoneUser;
             $r = EmilyOfficeNP::getOfficeKey($office->description);
                $order->addressUser = $r['description'];
             
            if($order->save()) {
                     
                     $this->saveOrderItems($session['cart'], $order->id);
                      $dostavka->idOrder = $order->id;
                      $dostavka->cityFrom = "db5c88f5-391c-11dd-90d9-001a92567626";
                      $dostavka->cityTo = $cities->description;
                      $dostavka->officeNP = $office->description;
                       
                      $dostavka->contactUser = mb_convert_case($dostavka->contactUser, MB_CASE_TITLE);
                                       
                      $dostavka->save();
                     
                     $this->createEN($order->id, $order->sum, $ar);
                
             
                return $this->refresh();    
                 }
                 else {
                Yii::$app->session->setFlash('error_order', 'Замовлення не може бути прийняте! (помилка при оформленні) ');
            }
            }
            return $this->render('view', compact('session', 'order', 'cities', 'office', 'dostavka'));
                  
     }

          /**
     * Save the order elements in the database
     * @param array $items the cart
     * @param int $order_id the ID order
     */
    protected function saveOrderItems($items, $order_id) {
        
        foreach ($items as $id => $item) {
            $order_items = new OrderItems();
            $order_items->order_id = $order_id;
            $order_items->product_id = $id;
            $order_items->name = $item['name'];
            $order_items->price = $item['price'];
            $order_items->qty_item = $item['qty'];
            $order_items->sum_item = $item['qty'] * $item['price'];
            $order_items->save();
        }
    }
    /**
     * Save Internet Document in the database
     * @param int $id the id order
     * @param int $summ the summ order (Cost)
     * @param array $order 
     * 
     */
    
    protected function createEN($id, $summ, $order){
         $city = Cities::getCityID($order['Cities']['description']);
         $cityName = $city->description;
         $office = EmilyOfficeNP::getOfficeKey($order['EmilyOfficeNP']['description']);
         $address = $office->number;
         $nameUser = $order['DostavkaNP']['contactUser'];
         $phone = (string)$order['DostavkaNP']['phoneUser'];
    
        $sendArray = DostavkaNP::arrayEN($summ, $cityName, $address, $nameUser, $phone);
        
         $result = DostavkaNP::createEN($sendArray);
        
         $document = new InternetDocument();
         $document->Ref = $result['data'][0]['Ref'];
         $document->CostOnSite = $result['data'][0]['CostOnSite'];
         $document->EstimatedDeliveryDate = $result['data'][0]['EstimatedDeliveryDate'];
         $document->IntDocNumber = $result['data'][0]['IntDocNumber'];
         $document->TypeDocument = $result['data'][0]['TypeDocument'];
         $document->idOrder = $id;
         if($document->save()) {
              Yii::$app->session->setFlash('success_order', 'Ваше замовлення прийняте! Вартість доставки приблизно '."$document->CostOnSite грн.");
                    $session = Yii::$app->session;
                    $session->open();
                 
                   $session->remove('cart');
                   $session->remove('cart.qty');
                   $session->remove('cart.sum');
         }
         else { 
             Yii::$app->session->setFlash('error_order', 'Замовлення не може бути прийняте! (помилка при оформленні)');
             return $this->refresh();
         }
        
     
    
     }
    
}
