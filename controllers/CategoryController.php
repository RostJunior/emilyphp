<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;
use app\controllers\AppController;
use app\models\Product;
use \app\models\Type;
/**
 * Description of CategoryController
 *
 * @author Rost
 */
class CategoryController extends AppController{
    /**
     * Displays index page of site.
     * @var array $products the result is fetching all the products from the database
     * @var array $typs the result is fetching all the types of product from the database
     * @return mixed
     * @see index page
     */
    public function actionIndex() {
        $products = Product::find()->with('typs')->all();
        $typs = Type::find()->all();
         $keywords = 'канзаши, обручи, резинки, заколки, повъязки, канзаші, emily, emili';
         $description = 'Emily Украшения для волос в стиле канзаши: Обручи, резинки, брошки, заколки, повъязки.';
        $this->setMeta('Emily | Прикраси', $keywords, $description);
        return $this->render('index', compact('products', 'typs'));
    }
}
