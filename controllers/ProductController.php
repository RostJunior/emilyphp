<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;
use app\controllers\AppController;
use app\models\Product;
use Yii;
/**
 * Description of ProductController
 *
 * @author Rost
 */
class ProductController extends AppController {
     /**
     * Displays a page with information about the product.
     * 
     * @return mixed
     */
     public function actionInfo() {
        $id = Yii::$app->request->get('id');
        $product = Product::findOne($id);
        if(empty($product)) throw new \yii\web\HttpException(404, 'Такого товара нет!');
         $this->setMeta('EMILY | Інфо ' . $product->nameProduct);
        
        return $this->render('info', compact('product'));
    }
}
