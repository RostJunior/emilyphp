<?php
/**
 * class behavior management site
 */
namespace app\controllers;

use Yii;
use yii\data\Pagination;
use app\models\Vk;
use yii\filters\AccessControl;
use app\controllers\AppController;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends AppController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @var array $products the result is fetching all the products from the database
     * @return mixed
     * @see Index page
     */
    public function actionIndex()
    {
        $products = Product::find()->all();
        $this->setMeta('Emily'); //set title of page
        return $this->render('index', compact('products'));
    }
    
    public function actionLogsuccess()
    {
        $this->setMeta('Emily | login');
        return $this->render('logsuccess');
    }
    
    public function actionCities()
    {
        
        return $this->render('citiesnp');
    }
    
       public function actionRegistr()
    {
           $this->setMeta('Emily | Registration');
        $model = new \app\models\User();
        $session = Yii::$app->session;
        $session->open();
         if ($model->load(Yii::$app->request->post())) {
              $session = Yii::$app->session;
               $session->open();
             if ($model->passwordUsers !== $model->confirm)
             {
                  Yii::$app->session->setFlash('error_reg', 'Поля для введення пароля не відповідають один одному');
                   return $this->refresh();
             } 
             if ($model->findEmail($model->emailUsers)) {
                  Yii::$app->session->setFlash('error_reg', 'e-Mail вже існує!');
                            
                return $this->refresh();
               }
               
               $model->roles_idRoles = '10002';
               $model->passwordUsers = Yii::$app->getSecurity()->generatePasswordHash($model->passwordUsers);
               $model->save();
           Yii::$app->session->setFlash('succes_reg', 'Дякуємо за реєстрацію! Тепер можете увійти під своїм логіном і паролем!'); 
           
           return $this->render('logsuccess', compact('session'));
        }
        
        return $this->render('registration', compact('model', 'session'));
    }
    
     public function actionOffice()
    {
        
        return $this->render('officenp');
    }
    

    /**
     * Login action.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        $this->setMeta('Emily | login');
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model2 = new LoginForm();
        if ($model2->load(Yii::$app->request->post()) && $model2->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model2' => $model2,
        ]);
    }

    /**
     * Logout action.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     * @return mixed
     * @see Contact page
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @see About page
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionVkauth()
    {
        
      return $this->redirect(Vk::autorizeVK());
    }
    
    public function actionGetauth() {
		
               if(!Yii::$app->request->get('code')) {
                    $message = "Помилка при авторизації користувача!";
                    return $this->render('error', compact('message'));
                }
		if(Yii::$app->request->get('error')) {
			$message = Yii::$app->request->get('error');
			return $this->render('error', compact('message'));
		}
       
             
		 $code = Yii::$app->request->get('code');
                 $access_token = Vk::getTokenVK($code);
                 $user_id = $access_token['user_id'];
                 $user_token = $access_token['access_token'];
                 $user = Vk::getUserVK($user_id, $access_token);
                 
                 //дальше реализуем сохранение в базу (код на хосте)
                /*
                 * Array
(
    [access_token] => код
    [expires_in] => 0
    [user_id] => 0000000
)
     //url auth VK
    public function actionVkauth()
    {
        $vk = new Vk();
      return $this->redirect($vk->autorizeVK());
    }
	//url auth Facebook
	  public function actionAuthfacebook()
    {
      return $this->redirect(Facebook::autorizeFacebook());
    }
   
     // Auth Vk (get code and get access_token)
     
  
    public function actionGetauth() {
		
               if(!Yii::$app->request->get('code')) {
                    $message = "Помилка при авторизації користувача!";
                    return $this->render('error', compact('message'));
                }
		if(Yii::$app->request->get('error')) {
			$message = Yii::$app->request->get('error');
			return $this->render('error', compact('message'));
		}
       
             
			 if(Yii::$app->request->get('code')) {
			 $vk = new Vk();
				$code = Yii::$app->request->get('code');
				
                 $access_token = $vk->getTokenVK($code);
                 $user_id = $access_token['user_id'];
                 $user_token = $access_token['access_token'];
				 $session = Yii::$app->session;
                 $session->open();
				 $session['user_id'] = $user_id;
				 $session['user_token'] = $user_token;
				return $this->redirect('getauth2');
			 }
                
	}
	
     // Auth Vk (get user id)
    
	public function actionGetauth2() {
		$session = Yii::$app->session;
        $session->open();
		$id =  $session['user_id'];
		return $this->render('testauth', compact('id'));
	}
	
     // Auth Vk (login user_vk, add Database new User)
    
	public function actionAuthvk() {
		if(Yii::$app->request->post()) {
				 $r = Yii::$app->request->post();
				$name = $r['first_name']." ".$r['last_name'];
				$model = new User();
				if($model->findVkid($r['uid'])) {
					 $user = $model->findVkid($r['uid']);
					 return Yii::$app->user->login($user);
					
				}else{
					
					$model->nameUsers = $name;
					$model->passwordUsers = Yii::$app->getSecurity()->generatePasswordHash($name."VKID@".$r['uid']);
					$model->telephoneNumberUsers = 'null'; 
					$model->confirm = Yii::$app->getSecurity()->generatePasswordHash($name."VKID@".$r['uid']);;
					$model->vk_id = $r['uid'];
					$model->emailUsers = "VKID@".$r['uid'];
					$model->roles_idRoles = '10002';
					if($model->save()) return Yii::$app->user->login($model);
					else return "don`t save";
				//	if($model->save()) return "SAVE";
				//	else return ("надо создать".$r['uid']);
					
				}
			 } 
			 if(Yii::$app->request->get()) {
				 return $this->redirect(Yii::$app->request->get());
			 }
			 return $this->render('/');
	}
	
	
     // Auth Facebook (get code and get access_token) 
    
	public function actionFaceauth() {
		
         if(!Yii::$app->request->get('code')) {
                    $message = "Помилка при авторизації користувача!";
                    return $this->render('error', compact('message'));
                }
			$code = Yii::$app->request->get('code');
			Facebook::getTokenFacebook($code);
			$token = Yii::$app->request->get('code');
			if(!$token) {
				$message = "Помилка при авторизації користувача!";
                    return $this->render('error', compact('message'));
			}
			$fuser = Facebook::getUser($token);
			debug($fuser);
}
                
                 * 
                 * 
                 * 
                 // page testauth
                 * 
                 * <div class="site-login container">
<h1>Зачекайте йде процес авторизації</h1> 
<div id="vk_auth2"></div>
<script type="text/javascript">
window.onload = function () {
 VK.init({apiId: 5953021});
 VK.Api.call('users.get', {user_ids: <?=$id?>, fields: ['id','city','first_name','last_name']}, function(r) {
  if(r.response) {
	  var res = [];
	  res = r.response[0];
	 
	  $.ajax({
			  
			type:'POST',
			url:'/site/authvk',
			data: res,
			success:function(rest) {
				if(!rest) alert('Щось пішло не так!');
				   window.location.href = "http://emily.hol.es/";
			},
			error: function() {
                        alert("ERROR_AUTH_VK!!");
                 }
		});

	 
   
  } 
});
}
</script>
                 *  */
	}
        //парсим сайт riveragold (перенаправляем на скрипт с кодом)
        public function actionGetposts()
    {
          return $this->render('getriveragold');
      
    }
    //получаем список постов и выводим в браузер с пагинацией
        public function actionPosts()
    {
           
         $query = \app\models\EmilyPosts::find(); 
         $pages = new Pagination(['totalCount' => $query->count(), 
            'pageSize' => 10, 'forcePageParam' => false, 'pageSizeParam' => false]);
       
        $posts = $query->offset($pages->offset)->limit($pages->limit)
                ->all();
         $this->setMeta('Emily | blog');
          return $this->render('blogs', compact('posts', 'pages'));
    
    }
    //выводим пост
    
     public function actionPostinfo()
    {
         $id = Yii::$app->request->get('id');
         $post = \app\models\EmilyPosts::findOne($id);
         if(empty($post)) {throw new \yii\web\HttpException(404, 'Пост не знайдено!');}
            return $this->render('postinfo', compact('post'));
    }
}
