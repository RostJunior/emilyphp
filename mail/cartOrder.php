<?php 
/**
 * view the content of the email
 */
use yii\helpers\Html;

?>
<h3>Доброго дня!</h3>
<p>
    Ваше замовлення №<?= $order->id?> на ім`я <?= $order->nameUser?> прийнято. <br/>
    Очікуйте дзвінка на номер  <?= $order->phoneUser?> найближчим часом. <br/>
    Дякуємо!<br/>
</p>
<h4>Інформація по замовленню:</h4>

    <div class="table-responsive">
        <table class="table table-hover table-striped" style="width: 450px;">
            <thead style="background-color: #e0c8dd;">
                <tr style="height: 35px;">
                 
                    <th>Назва</th>
                    <th>Кількість</th>
                    <th>Ціна</th>
                    <th>Сумма</th>
                   
                </tr>
            </thead>
            <tbody style="text-align: center; color: #0a4437; background-color: #e5f9e0;">
                               
                <?php foreach ($session['cart'] as $id=>$item):?>
                  <tr style="height: 38px;"> 
                     
                     <td><?= $item['name']?></td>
                      <td><?= $item['qty']?></td>
                       <td><?= $item['price']?></td>
                       <td><?= $item['qty'] * $item['price']?></td>
                      
               
                  </tr> 
                <?php endforeach;?>
                  <tr>
                      <td colspan="3">Итого:</td>
                      <td><?= $session['cart.qty']?> шт.</td>
                  </tr>
                   <tr>
                      <td colspan="3">На сумму:</td>
                      <td><?= $session['cart.sum']?> грн.</td>
                  </tr>
            </tbody>
            
        </table>
</div>



