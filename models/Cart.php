<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for to control the Cart.
 *
 * @property string $id
 * @property int $qty
 * @property array $product
 */

class Cart extends ActiveRecord {
    
    
    public function addToCart($product, $qty= 1) {
        if(isset($_SESSION['cart'][$product->idProduct])) {
            $_SESSION['cart'][$product->idProduct]['qty'] += $qty;
            
        } else {
            $_SESSION['cart'][$product->idProduct] = [
                'qty' => $qty,
                'name' => $product->nameProduct,
                'price' => $product->priceProduct,
                'img' => $product->namePhoto
            ];
        }
        $_SESSION['cart.qty'] = isset($_SESSION['cart.qty'])? $_SESSION['cart.qty'] + $qty : $qty;
        $_SESSION['cart.sum'] = isset($_SESSION['cart.sum'])? $_SESSION['cart.sum'] + $qty * $product->priceProduct : $qty * $product->priceProduct;
    } 
    
    public function recalc($id) {
       
         if(!isset($_SESSION['cart'][$id])) return false;
         $qtyMinus = $_SESSION['cart'][$id]['qty'];
         $sumMinus = $_SESSION['cart'][$id]['qty'] *  $_SESSION['cart'][$id]['price'];
         $_SESSION['cart.qty'] -= $qtyMinus;
         $_SESSION['cart.sum'] -= $sumMinus;
         unset($_SESSION['cart'][$id]);
         
    }
}