<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emily_cities".
 *
 * @property string $id
 * @property string $description
 * @property string $descriptionRu
 * @property string $ref
 * @property string $area
 * @property integer $cityID
 */
class Cities extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emily_cities';
    }
    
     public static function getCityID($id)
    {
        
         return  Cities::find()->where(['cityID' => $id])->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'descriptionRu', 'ref', 'area', 'cityID'], 'required'],
            [['cityID'], 'integer'],
            [['description', 'descriptionRu', 'ref', 'area'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Місто',
            'descriptionRu' => 'Город',
            'ref' => 'Ref',
            'area' => 'Area',
            'cityID' => 'CityID',
        ];
    }
    
    
}
