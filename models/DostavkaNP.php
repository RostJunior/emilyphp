<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dostavkaNP".
 *
 * @property integer $id
 * @property integer $idOrder
 * @property string $cityFrom
 * @property string $cityTo
 * @property string $officeNP
 * @property string $contactUser
 * @property string $phoneUser
 */
class DostavkaNP extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dostavkaNP';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['idOrder', 'cityFrom', 'cityTo', 'officeNP', 'contactUser', 'phoneUser'], 'required'],
            [['idOrder'], 'integer'],
            [['cityFrom', 'cityTo', 'officeNP', 'contactUser', 'phoneUser'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'idOrder' => 'Id Order',
            'cityFrom' => 'City From',
            'cityTo' => 'City To',
            'officeNP' => 'Office Np',
            'contactUser' => 'Контактна особа',
            'phoneUser' => 'Талефон контактної особи'
        ];
    }
    
    public static function arrayEN($summ, $cityName, $address, $nameUser, $phone) {
       return $res = [
        "apiKey" => "505c0b389f71f705bd081d753e9378b7",
        "modelName" => "InternetDocument",
        "calledMethod" => "save",
        "methodProperties" => [
         "NewAddress" => "1",
        "PayerType" => "Recipient",
        "PaymentMethod" => "Cash",
        "CargoType" => "Cargo",
        "Weight" => "0.5",
        "ServiceType" => "WarehouseWarehouse",
        "SeatsAmount"=> "1",
        "Description"=> "Інше",
        "Cost"=> "$summ",
        "CitySender"=> "db5c88f5-391c-11dd-90d9-001a92567626",
        "Sender"=> "630b61e9-fe4d-11e6-8ba8-005056881c6b",
        "SenderAddress"=> "16922806-e1c2-11e3-8c4a-0050568002cf",
        "ContactSender"=> "ebdaa999-010b-11e7-8ba8-005056881c6b",
        "SendersPhone"=> "380991234567",
        "RecipientCityName"=> "$cityName",
        "RecipientArea"=> "",
        "RecipientAreaRegions"=> "",
        "RecipientAddressName"=> "$address",
        "RecipientHouse"=> "",
        "RecipientFlat"=> "",
        "RecipientName"=> "$nameUser",
        "RecipientType"=> "PrivatePerson",
        "RecipientsPhone"=> "$phone"
        ],
];
    }
    
    public static function createEN($arr) {
        $ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'http://testapi.novaposhta.ua/v2.0/en/save_warehouse/json/');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arr));
		curl_setopt($ch, CURLOPT_POST, 1);
		
		$response = curl_exec($ch);
		curl_close($ch);
                
                $result = is_array($response)
				? $response
				: json_decode($response, 1);
                
                return $result;
    }
    
}
