<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emily_officeNP".
 *
 * @property integer $id
 * @property string $siteKey
 * @property string $description
 * @property string $descriptionRu
 * @property string $typeOfWarehouse
 * @property string $refOffice
 * @property integer $number
 * @property string $cityRef
 * @property string $cityDescription
 * @property string $cityDescriptionRu
 */
class EmilyOfficeNP extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emily_officeNP';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['siteKey', 'description', 'descriptionRu', 'typeOfWarehouse', 'refOffice', 'number', 'cityRef', 'cityDescription', 'cityDescriptionRu'], 'required'],
            [['number'], 'integer'],
            [['siteKey', 'description', 'descriptionRu', 'typeOfWarehouse', 'refOffice', 'cityRef', 'cityDescription', 'cityDescriptionRu'], 'string', 'max' => 255],
        ];
    }
    
     public static function getOfficeKey($id)
    {
        
         return EmilyOfficeNP::find()->where(['siteKey' => $id])->one();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            
            'siteKey' => 'Site Key',
            'description' => 'Відділення',
            'descriptionRu' => 'Description Ru',
            'typeOfWarehouse' => 'Type Of Warehouse',
            'refOffice' => 'Ref Office',
            'number' => 'Number',
            'cityRef' => 'City Ref',
            'cityDescription' => 'City Description',
            'cityDescriptionRu' => 'City Description Ru',
        ];
    }
}
