<?php

namespace app\models;

use Yii;
use phpQuery;
/**
 * This is the model class for table "emily_posts".
 *
 * @property integer $id
 * @property string $name_post
 * @property string $link_post
 * @property string $short_desc
 * @property string $post_all
 */
class EmilyPosts extends \yii\db\ActiveRecord
{
 
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emily_posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_all', 'short_desc'], 'string'],
            [['name_post', 'link_post'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_post' => 'Name Post',
            'link_post' => 'Link Post',
            'short_desc' => 'Short Desc',
            'post_all' => 'Post All',
        ];
    }
    
    public static function getPage() {
        
        $curl = curl_init('https://riveragold.com/posts');
        curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt ($curl, CURLOPT_HEADER, 0);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);

            $page = curl_exec($curl);
            curl_close($curl);
            return $page;
    }
    
    public static function getPageUrl($url) {
        
        $curl = curl_init($url);
        curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt ($curl, CURLOPT_HEADER, 0);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);

            $page = curl_exec($curl);
            curl_close($curl);
            return $page;
    }
    
    public static function getNews($page) {
         $pq = phpQuery::newDocument($page);
         $newsAll = $pq->find('.news');
         return $newsAll;
    }
    
    public static function setPosts($arr) {
            
            
            $res = TRUE;
           foreach($arr as $post) {
                   
                   $obj = new EmilyPosts();
                   
                    $obj->link_post = pq($post)->find('.ttlnlin a')->attr('href');
                    $obj->name_post =  pq($post)->find('.ttlnlin span')->text();
                    $obj->short_desc = pq($post)->find('noindex')->text();
                    $obj->post_all =  $obj->getDescription($obj->link_post);
                    if(!$obj->save()) {
                        $res = FALSE;
                    }
                 
                  }
             return $res;
    }
    
    public static function getPagination($page) {
         $pages = [];
         $pq = phpQuery::newDocument($page);
         $links = $pq->find('.pagination li a');
         foreach ($links as $str) {
            $pages[] = pq($str)->attr('href');
            }
         $linkPages = array_unique($pages);  
         return $linkPages;
    }
    
    public function getDescription($url) {
        $curl = curl_init($url);
        curl_setopt ($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt ($curl, CURLOPT_HEADER, 0);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt ($curl, CURLOPT_SSL_VERIFYHOST, 0);

            $page = curl_exec($curl);
            curl_close($curl);
             $pq = phpQuery::newDocument($page);
             $desc = $pq->find('.info-c');
             $img =  $desc->find('img');
             foreach ($img as $i) {
                 $h = pq($i)->attr('src');
                 $href = "https://riveragold.com".$h;
                 pq($i)->attr('src',  $href);
             }
             
             return $desc->html();
    }
    
    public static function autoIncr() {
        
      Yii::$app->db->createCommand("alter table `emily_posts` AUTO_INCREMENT = 1")->execute();
    }
}
