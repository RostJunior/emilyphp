<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "internetDocument".
 *
 * @property integer $id
 * @property string $Ref
 * @property integer $CostOnSite
 * @property string $EstimatedDeliveryDate
 * @property integer $IntDocNumber
 * @property string $TypeDocument
 * @property string $idOrder
 */
class InternetDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'internetDocument';
    }
    
    public static function getNumberOfID($id) {
        return InternetDocument::find()->where(['idOrder' => $id])->one();
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['Ref', 'CostOnSite', 'EstimatedDeliveryDate', 'IntDocNumber', 'TypeDocument', 'idOrder'], 'required'],
            [['CostOnSite', 'IntDocNumber', 'idOrder'], 'integer'],
            [['Ref', 'EstimatedDeliveryDate', 'TypeDocument'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'Ref' => 'Ref',
            'CostOnSite' => 'Cost On Site',
            'EstimatedDeliveryDate' => 'Estimated Delivery Date',
            'IntDocNumber' => 'Int Doc Number',
            'TypeDocument' => 'Type Document',
        ];
    }
}
