<?php

namespace app\models;

use Yii;
use yii\db\Expression;
use \yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use app\models\OrderItems;
/**
 * This is the model class for table "emily_order".
 *
 * @property string $id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $qty
 * @property double $sum
 * @property string $status
 * @property string $nameUser
 * @property string $emailUser
 * @property string $phoneUser
 * @property string $addressUser
 * @property string $dostavka
 */
class Order extends ActiveRecord
{
    /**
     * @return tableName
     */
    public static function tableName()
    {
        return 'emily_order';
    }
    
    public function behaviors() {
        return [
         [
            'class' => TimestampBehavior::className(),
             'createdAtAttribute' => 'created_at',
            'updatedAtAttribute' => 'updated_at',
             'value' => new Expression('NOW()'),
         ],
     ];
    }

    
    
    public function getOrderItem() 
    {
        return $this->hasMany(OrderItems::className(), ['order_id' => 'id']);
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            
            ['nameUser', 'required', 'message' => 'Введіть ім`я'],
            ['emailUser', 'required', 'message' => 'Введіть email'],
            ['phoneUser', 'required', 'message' => 'Введіть телефон'],
            
            [['created_at', 'updated_at'], 'safe'],
            [['qty'], 'integer'],
            [['sum'], 'number'],
            [['status'], 'boolean'],
            [['nameUser', 'emailUser', 'phoneUser', 'addressUser', 'dostavka'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
           
            'nameUser' => 'Ім`я',
            'emailUser' => 'E-mail',
            'phoneUser' => 'Телефон',
            
        ];
    }
}
