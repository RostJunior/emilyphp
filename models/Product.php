<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "emily_product".
 *
 *  @author Rost
 */

class Product extends ActiveRecord {
   public static function tableName() {
        return 'emily_product';
    }
    
    public function getTyps() {
        return $this->hasOne(Type::className(), ['idType' => 'type_idType']);
    }
}
