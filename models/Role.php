<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;
use yii\db\ActiveRecord;
use app\models\User;
/**
 *  This is the model class for table "emily_users".
 *
 * @author Rost
 */
class Role extends ActiveRecord{
    
     public static function tableName()
    {
        return 'emily_users';
    }
    
    public function getUsers() {
        return $this->hasMany(User::className(), ['idRoles' => 'roles_idRoles']);
    }
    
    
}
