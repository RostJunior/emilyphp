<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\models;
use yii\db\ActiveRecord;
use app\models\Product;

/**
 *  This is the model class for table "emily_type".
 *
 * @author Rost
 */
class Type extends ActiveRecord {
    
    public static function tableName() {
        return 'emily_type';
    }
    
    public function getProducts() {
        return $this->hasMany(Product::className(), ['type_idType' => 'idType']);
    }
}
