<?php

namespace app\models;
use \yii\db\ActiveRecord;
use app\models\Role;

    /**
     *  This is the model class for table "emily_users".
     */

class User extends ActiveRecord implements \yii\web\IdentityInterface
{
    
    public $confirm;
    const ROLE_ADMIN = 10001;


    public static function tableName()
    {
        return 'emily_users';
    }

   public function getRole() {
        return $this->hasOne(Role::className(), ['roles_idRoles' => 'idRoles']);
    }
    
    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    
    public function findEmail($email)
    {
        return $this->find()->where(['emailUsers' => $email])->one();
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        /* не используем пока
         * foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
         * 
         */
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
       return static::findOne(['nameUsers' => $username]);
    }
    
    public static function findByUserEmail($email)
    {
       return static::findOne(['emailUsers' => $email]);
    }
    
     public function rules()
    {
        return [
           
            [['nameUsers', 'emailUsers', 'passwordUsers', 'confirm', 'telephoneNumberUsers'], 'required'],
            [['telephoneNumberUsers'], 'string', 'max' => 20],
            [['nameUsers', 'cityUsers'], 'string', 'max' => 50],
            [['passwordUsers', 'auth_key'], 'string', 'max' => 255],
            [['passwordUsers'], 'string', 'min' => 3],
             [['auth_key'], 'string', 'max' => 255],
            [['cityUsers', 'emailUsers'], 'string', 'max' => 150],
            [['emailUsers'], 'unique'], 
            	 [['vk_id'], 'string'], 
            ['role', 'in', 'range' => [self::ROLE_USER, self::ROLE_ADMIN]],
           
        ];
    }
        
    public static function isUserAdmin($username)
{
    if (static::findOne(['emailUsers' => $username, 'roles_idRoles' => self::ROLE_ADMIN]))
    {
        return true;
    } else {
        return false;
    }
}
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idUsers' => 'Id',
            'nameUsers' => 'Ім`я',
            'adressUsers' => 'Адреса',
            'cityUsers' => 'Місто',
            'emailUsers' => 'e-Mail',
            'passwordUsers' => 'Пароль',
            'confirm' => 'Повторіть пароль',
            'telephoneNumberUsers' => 'Телефон'
        ];
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->idUsers;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
     //   return $this->password === $password;
        
        return \Yii::$app->security->validatePassword($password, $this->passwordUsers);
                
    }
    
    public function generateAuthKey() {
        
       $this->auth_key = \Yii::$app->security->generateRandomString();
        
    }
    
    public function sizePassword($password) {
        if (mb_strlen($password) < 3 ) {
            return false;
        }else
        return true;
    }
}
