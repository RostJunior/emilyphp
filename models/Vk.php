<?php

namespace app\models;

use Yii;
use yii\helpers\Json;

class Vk extends \yii\db\ActiveRecord 
{
    
    
    
    public static function autorizeVK() {
        
      $method = 'https://oauth.vk.com/authorize?';
        $params = [
            'client_id' => Yii::$app->params['idvk'],
            'display' => 'page',
            'redirect_uri' => Yii::$app->params['redirect_uri'],
            'scope' => 'offline',
            'response_type' => 'code',
            'v' => '5.52'
        ];
        $url = $method.urldecode(http_build_query($params));
        return $url;
    }
    
    public static function getTokenVK($code) {
        
             $method = 'https://oauth.vk.com/access_token?';
             $params = [
            'client_id' => Yii::$app->params['idvk'],
            'client_secret' => Yii::$app->params['keyvk'],
            'redirect_uri' => Yii::$app->params['redirect_uri'],
            'code' => $code,
            
            
        ];
        
                $ku = curl_init();
                $url = $method.urldecode(http_build_query($params));
		curl_setopt($ku,CURLOPT_URL,$url);
		curl_setopt($ku,CURLOPT_RETURNTRANSFER,TRUE);
		$result = curl_exec($ku);
		curl_close($ku);
		$ob = json_decode($result, true);
                return $ob;
    } 
    
    public static function getUserVK($id, $token) {
       
            $method = 'https://api.vk.com/method/users.get?';
            $params = [
                'user_ids' => $id,
                'access_token' => $token,
                'v' => '5.63'
            ];
            
                $ku = curl_init();
                $url = $method.urldecode(http_build_query($params));
		curl_setopt($ku,CURLOPT_URL,$url);
		curl_setopt($ku,CURLOPT_RETURNTRANSFER,TRUE);
		$result = curl_exec($ku);
		curl_close($ku);
		$ob = json_decode($result, true);
                return $ob;
                
                /*
                    $get_params = http_build_query($params);
$result = json_decode(file_get_contents('https://api.vk.com/method/users.get?'. $get_params));
 return $result -> response[0];
                 */
    }
}
