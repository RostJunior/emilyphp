<?php

namespace app\modules\admin\controllers;
use \yii\filters\AccessControl;
use yii\web\Controller;
use app\models\User;
use Yii;
/**
 * AdminSiteControlle for the `admin` module
 */
class AdminSiteController extends Controller
{
    public function behaviors() {
        return [
        'access' => [
            'class' => AccessControl::className(),
            'rules' => [ [
                'allow' => true,
                   'roles' => ['@'],
                   'matchCallback' => function ($rule, $action) {
                       return User::isUserAdmin(Yii::$app->user->identity->emailUsers);
                   }]
            ]
            
        ]
        ];
    }
}
