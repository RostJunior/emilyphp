<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\EmilyOrder;
use yii\data\ActiveDataProvider;
use app\modules\admin\controllers\AdminSiteController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \app\models\InternetDocument;

/**
 * OrderController implements the CRUD actions for EmilyOrder model.
 */
class OrderController extends AdminSiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
                
        ];
    }

    /**
     * Lists all EmilyOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => EmilyOrder::find(),
            'pagination' => [
                'pageSize' => 10
            ],
            'sort' => [
                'defaultOrder' => [
                    'status' => SORT_ASC
                ]
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmilyOrder model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        
        return $this->render('view', [
            'model' => $this->findModel($id),
           'document' => \app\models\InternetDocument::getNumberOfID($id),
        ]);
    }

    /**
     * Creates a new EmilyOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmilyOrder();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EmilyOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
         
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EmilyOrder model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmilyOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return EmilyOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmilyOrder::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
     public function actionEprint($id)
    {
       $document = InternetDocument::getNumberOfID($id);
       $a = "http://testapi.novaposhta.ua/orders/printDocument/orders[]/"."$document->IntDocNumber"."/type/pdf/apiKey/505c0b389f71f705bd081d753e9378b7";
         if ($document !== null) { return $this->redirect($a);}
         else{ throw new NotFoundHttpException('По даному замовленню документ відсутній!');}
         
         
    }
}
