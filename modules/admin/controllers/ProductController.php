<?php

namespace app\modules\admin\controllers;

use Yii;
use app\modules\admin\models\EmilyProduct;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\modules\admin\controllers\AdminSiteController;

/**
 * ProductController implements the CRUD actions for EmilyProduct model.
 */
class ProductController extends AdminSiteController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all EmilyProduct models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => EmilyProduct::find(),
             'pagination' => [
                'pageSize' => 10
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single EmilyProduct model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new EmilyProduct model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new EmilyProduct();
       
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
             $model->file = UploadedFile::getInstance($model, 'file');
            if ($model->file) {
              
                      $path = Yii::$app->params['pathImg'];
                      $this->createDirectory($path);
                      $model->file->name = "prod_".date('his').'.'.$model->file->extension;
                      $n = "prod_".date('his').'.'.$model->file->extension;
                       $model->namePhoto =  $n;
                
                  $model->save();
                    $model->file->saveAs($path.$model->file);
                    $this->addWaterMark($path, $model->file->name);
                
                return $this->redirect(['view', 'id' => $model->idProduct]); 
               
            }
            else {
                 $model->namePhoto = $path = Yii::$app->params['noPhoto'];
                 $model->save();
                 return $this->redirect(['view', 'id' => $model->idProduct]);  
            }
               
              
           
           
         //   return $this->redirect(['view', 'id' => $model->idProduct]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing EmilyProduct model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idProduct]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing EmilyProduct model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the EmilyProduct model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return EmilyProduct the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = EmilyProduct::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function createDirectory($path) {   
    
    if (file_exists($path)) {  
     //  echo "The directory {$path} exists"; 
    } else {  
        mkdir($path, 0775, true);  
     //   echo "The directory {$path} was successfully created.";
    }
}

    function addWaterMark($path, $name) {
        $file = $path.$name;
        list($source_image_width, $source_image_height, $source_image_type) = getimagesize($file);
        
        switch ($source_image_type) {
            case 1: // картинка *.gif
                $source_image = imagecreatefromgif($file);
                 break;
            case 2: // картинка *.jpeg, *.jpg
                $source_image = imagecreatefromjpeg($file);
                 break;
            case 3: // картинка *.png
                $source_image = imagecreatefrompng($file);
                break;
            default:
                return false; 
        }
         $watermark_image = imagecreatefrompng(Yii::$app->params['watermark']);
        $watermark_width = imagesx($watermark_image);
        $watermark_height = imagesy($watermark_image);
        $result = imagecreatetruecolor($source_image_width, $source_image_height);
        imagealphablending($result, true);
        imagesavealpha($result, true);
        imagecopyresampled($result, $source_image, 0, 0, 0, 0, $source_image_width, $source_image_height, $source_image_width, $source_image_height);
      
        imagecopy($result, $watermark_image, ($source_image_width/2) - $watermark_width/2, 
                ($source_image_height/2) - $watermark_height/2, 0, 0, $watermark_width, $watermark_height);
        imagejpeg($result, $file, 100);
// Уничтожение всех временных ресурсов
        imagedestroy($source_image);
        imagedestroy($watermark_image);
    }
}
