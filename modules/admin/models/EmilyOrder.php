<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\admin\models\OrderItems;

/**
 * This is the model class for table "emily_order".
 *
 * @property string $id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $qty
 * @property double $sum
 * @property string $status
 * @property string $nameUser
 * @property string $emailUser
 * @property string $phoneUser
 * @property string $addressUser
 * @property string $dostavka
 */
class EmilyOrder extends \yii\db\ActiveRecord
{
    public function behaviors()
{
    return [
        'timestamp' => [
            'class' => \yii\behaviors\TimestampBehavior::className(),
            'attributes' => [
                \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                \yii\db\ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
            ],
            'value' => function() { return date('Y-m-d H:i:s');},
        ],
    ];
} 
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emily_order';
    }
    
    public function getOrderItem() 
    {
        return $this->hasMany(OrderItems::className(), ['order_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'updated_at', 'qty', 'sum', 'nameUser', 'emailUser', 'phoneUser'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['qty'], 'integer'],
            [['sum'], 'number'],
            [['status'], 'string'],
            [['nameUser', 'emailUser', 'phoneUser', 'addressUser', 'dostavka'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID замовлення',
            'created_at' => 'Дата створення',
            'updated_at' => 'Дата оновлення',
            'qty' => 'Кількість товарів',
            'sum' => 'Сума замовлення',
            'status' => 'Статус',
            'nameUser' => 'Замовник',
            'emailUser' => 'Email',
            'phoneUser' => 'Телефон',
            'addressUser' => 'Адреса',
            'dostavka' => 'Вид доставки',
        ];
    }
}
