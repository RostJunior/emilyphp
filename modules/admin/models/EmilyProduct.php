<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "emily_product".
 *
 * @property string $idProduct
 * @property string $nameProduct
 * @property string $priceProduct
 * @property string $type_idType
 * @property string $description
 * @property string $namePhoto
 *
 * @property EmilyType $typeIdType
 */
class EmilyProduct extends \yii\db\ActiveRecord
{
    public $file;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emily_product';
    }
    
    public function getType() {
        return $this->hasOne(EmilyType::className(), ['idType' => 'type_idType']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['priceProduct', 'type_idType'], 'integer'],
            [['type_idType', 'priceProduct', 'nameProduct', 'description'], 'required'],
            [['nameProduct'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 255],
            [['namePhoto'], 'string', 'max' => 80],
            [['file'], 'file', 'extensions' => 'png, jpg'],
            [['type_idType'], 'exist', 'skipOnError' => true, 'targetClass' => EmilyType::className(), 'targetAttribute' => ['type_idType' => 'idType']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idProduct' => 'Id Продукту',
            'nameProduct' => 'Назва',
            'priceProduct' => 'Ціна',
            'type_idType' => 'Тип',
            'description' => 'Опис',
            'namePhoto' => 'Фото',
            'file' => 'Нове фото'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTypeIdType()
    {
        return $this->hasOne(EmilyType::className(), ['idType' => 'type_idType']);
    }
    
    
}
