<?php

namespace app\modules\admin\models;

use Yii;

/**
 * This is the model class for table "emily_type".
 *
 * @property string $idType
 * @property string $nameType
 *
 * @property EmilyProduct[] $emilyProducts
 */
class EmilyType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emily_type';
    }
    
    public function getProducts() {
        return $this->hasMany(EmilyProduct::className(), ['type_idType' => 'idType']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nameType'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'idType' => 'Id',
            'nameType' => 'Назва',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmilyProducts()
    {
        return $this->hasMany(EmilyProduct::className(), ['type_idType' => 'idType']);
    }
}
