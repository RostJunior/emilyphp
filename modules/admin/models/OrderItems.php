<?php

namespace app\modules\admin\models;

use Yii;
use app\modules\admin\models\EmilyOrder;

/**
 * This is the model class for table "emily_orderItems".
 *
 * @property string $id
 * @property string $order_id
 * @property string $product_id
 * @property string $name
 * @property double $price
 * @property integer $qty_item
 * @property double $sum_item
 */
class OrderItems extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emily_orderItems';
    }
    
    
    public function getOrder() 
    {
        return $this->hasOne(EmilyOrder::className(), ['id' => 'order_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'name', 'price', 'qty_item', 'sum_item'], 'required'],
            [['order_id', 'product_id', 'qty_item'], 'integer'],
            [['price', 'sum_item'], 'number'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Номер замовлення',
            'product_id' => 'ID товару',
            'name' => 'Назва',
            'price' => 'Ціна',
            'qty_item' => 'К-сть',
            'sum_item' => 'Сума',
        ];
    }
}
