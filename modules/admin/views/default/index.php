
<?php
use yii\helpers\Url;
use yii\helpers\Html;
?>


<div class="admin-default-index">
     <div class="admin-navigation">
        <?= Html::a('Обновити список міст', ['/site/cities'], ['class' => 'btn btn-success']) ?>
        
        
        <?= Html::a('Обновити список відділень "Нова пошта"', ['/site/office'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Обновити пости з сайту riveragold.com', ['/site/getposts'], ['class' => 'btn btn-success']) ?>
         <?= Html::a('Список замовленнь', ['/admin/order'], ['class' => 'btn btn-success']) ?>
         
         <?= Html::a('Список товарів', ['/admin/product'], ['class' => 'btn btn-success']) ?>
         <?= Html::a('Список типів', ['/admin/typs'], ['class' => 'btn btn-success']) ?>
     </div>
    
                        
                        
                       
</div>

<div class="clear"></div>

