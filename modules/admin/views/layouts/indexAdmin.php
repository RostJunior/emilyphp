<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Админка | <?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
      <?php
        $this->registerJsFile('js/html5shiv.js', 
            ['position' => \yii\web\View::POS_HEAD, 'condition' => 'lte IE9']);
    
    ?>  
    <?php
        $this->registerJsFile('js/respond.min.js', 
            ['position' => \yii\web\View::POS_HEAD, 'condition' => 'lte IE9']);
    
    ?> 

  
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/images//favicon.ico" type="image/x-icon">



</head>


<body>
<?php $this->beginBody() ?>
    <div id="boxWrapp" class="box-100">
    <div class="clearfix">
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#NavCol">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand bg-primary" href="<?= \yii\helpers\Url::to(['/'])?>">EMILY</a>
        </div>
        <div class="logout">
               <?php if(!Yii::$app->user->isGuest):?>
	<a  href="<?= \yii\helpers\Url::to(['/site/logout'])?>"><i class="fa fa-user"></i>
	<?= Yii::$app->user->identity['nameUsers']?> Выход</a><br/>
<?php endif;?>
            
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        
        
        <div class="mainmenu collapse navbar-collapse" id="NavCol1">
         <ul class="nav navbar-nav1 navbar-right">
           
            <li><a href="<?= \yii\helpers\Url::to(['/admin'])?>">Головна</a></li>
            
                
            <li><a href="<?= \yii\helpers\Url::to(['/admin/order'])?>" >Замовлення</a></li>
             <li><a href="<?= \yii\helpers\Url::to(['/admin/product'])?>" >Продукти</a>
                 <ul class="menu-product">
                     <li><a href="<?= \yii\helpers\Url::to(['/admin/product/create'])?>" >Створити</a></li>
                     <li><a href="<?= \yii\helpers\Url::to(['/admin/product'])?>" >Показати</a></li>
                 </ul>

             </li>
             <li><a href="<?= \yii\helpers\Url::to(['/admin/typs'])?>" >Категорії</a>
                 <ul class="menu-product">
                     <li><a href="<?= \yii\helpers\Url::to(['/admin/typs/create'])?>" >Створити</a></li>
                     <li><a href="<?= \yii\helpers\Url::to(['/admin/typs'])?>" >Показати</a></li>
                 </ul>

             </li>
            
          </ul>
       
         
        </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
 </div> 
        
    <div class="admin-page clear">    
        <?= $content ?>
        </div> 
   </div>       
   
    <!-- Footer -->
    <footer class="bg-black">
      <div class="container">
        <div class="row">
			<div class="col-md-6 ">
				<div class="cp-right">
					<p>&copy; 2015 <a href="#" class="color-primary linear">@Rost@</a>.Reserved. </p>
				</div><!-- end build -->
			</div><!-- end col -->
			
			<div class="col-md-6 text-right">
		
			<ul class="list-inline">
			<li><a href="http://vk.com/id289852074" class="socIcon color-primary linear"><i class="fa fa-vk fa-2x"></i></a></li>
			<li><a href="#" class="socIcon color-primary linear"><i class="fa fa-twitter fa-2x"></i></a></li>
			<li><a href="#" class="socIcon color-primary linear"><i class="fa fa-dribbble fa-2x"></i></a></li>
			</ul>
		
			</div>
        </div>
      </div>
    </footer>
    <!-- /Footer -->
 <?php $this->endBody() ?>


</body>
</html>
<?php $this->endPage() ?>
