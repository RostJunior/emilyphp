<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\EmilyOrder */

$this->title = 'Create Emily Order';
$this->params['breadcrumbs'][] = ['label' => 'Emily Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emily-order-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
