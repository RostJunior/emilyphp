<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Список замовлень';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emily-order-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Створити замовлення', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'created_at',
            'updated_at',
            'qty',
            'sum',
            [
                'attribute' => 'status',
                'value' => function($data) {
        return !$data->status ? '<span class="text-danger">Активне</span>' : '<span class="text-success">Зроблено</span>';
                },
                 'format' => 'html'
            ],
            'dostavka',
            // 'status',
            // 'nameUser',
            // 'emailUser:email',
            // 'phoneUser',
            // 'addressUser',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
