<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\EmilyOrder */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Emily Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emily-order-view">

    <h1>Замовлення № <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        
        <?= Html::a('Видалити', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Список замовленнь', ['index'], ['class' => 'btn btn-success']) ?>
        <?php
        if($model->dostavka == "NovaPoshta") {
      echo  Html::a(' Друкувати накладну', ['eprint', 'id' => $model->id], ['class' => 'btn  btn-info', 'target' => '_blank']);
        }?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'created_at',
            'updated_at',
            'qty',
            'sum',
         //   'status',
              [
                'attribute' => 'status',
                'value' => !$model->status ? '<span class="text-danger">Активне</span>' : '<span class="text-success">Зроблено</span>'
                ,
                 'format' => 'html',
            ],
            'nameUser',
            'emailUser:email',
            'phoneUser',
            'addressUser',
            'dostavka'
        ],
    ]) ?>
    
     <h2>Товари в замовленні:</h2>
     
     <?php
     $it = $model->orderItem; ?>
        <div id="cart-order" class="table-bordered">
        <table class="table">
            <thead>
                <tr>
                    <th>ID Товару</th>
                    <th>Назва</th>
                    <th>Ціна</th>
                    <th>Кількість</th>
                    <th>Сумма</th>
                        
                </tr>
            </thead>
            <tbody>
                <?php foreach ($it as $item):?>
                 <tr>
                     <td><?= $item['product_id']?></td>
                     <td><a href="<?= Url::to(['/product/info', 'id'=>$item['product_id']])?>"><?= $item['name']?></a></td>
                      <td><?= $item['price']?></td>
                       <td><?= $item['qty_item']?></td>
                        <td><?= $item['price'] * $item['qty_item']?></td>
                       
               
                  </tr> 
                <?php endforeach;?>
                 
            </tbody>
            
        </table>
</div>
     
     
    
</div>

