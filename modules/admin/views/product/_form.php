<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use mihaildev\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\EmilyProduct */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emily-product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'nameProduct')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'priceProduct')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'type_idType')->dropDownList(
            \app\modules\admin\models\EmilyType::find()->select(['nameType', 'idType'])
            ->indexBy('idType')->column(), ['promt' => 'Виберіть тип']) ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(),[
    'editorOptions' => [
        'preset' => 'full', //разработанны стандартные настройки basic, standard, full данную возможность не обязательно использовать
        
    ]]); ?>
    
   
    <?php if($model->namePhoto) :?>
    <p>Фото</p>
       <?= Html::img("@web/images/portfolio/{$model->namePhoto}", ['alt' => $model->namePhoto, 'width' => '90px']);?>
   <br/>
    <?php endif;?>    
   

    <?= $form->field($model, 'file')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
