<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\EmilyProduct */

$this->title = 'Create Emily Product';
$this->params['breadcrumbs'][] = ['label' => 'Emily Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emily-product-create  emily-order-view">
    <p>
         <?= Html::a('Список товарів', ['index'], ['class' => 'btn btn-success']) ?>
    </p>
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
