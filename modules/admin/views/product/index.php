<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Emily | Товари';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emily-product-index emily-order-view">

    <h1>Список товарів:</h1>

    <p>
        <?= Html::a('Створити новий продукт', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'idProduct',
            'nameProduct',
            'priceProduct',
            //'type_idType',
            [
                'attribute' => 'type_idType',
                'value' => function ($model) {
                    return $model->typeIdType->nameType;
                }
            ],
         //   'description:html',
            [
                
                'attribute' => 'namePhoto',
                'contentOptions' =>['class' => 'table_photo','style'=>'padding:2px;'],
                'value' => function ($model) {
            return \yii\helpers\Html::img("@web/images/portfolio/{$model->namePhoto}", ['alt' => $model->namePhoto, 'width' => '60px']);
        },
               
                 'format' => 'html',
           
            ],
         //   'namePhoto',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
