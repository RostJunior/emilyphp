<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\EmilyProduct */

$this->title = $model->idProduct;
$this->params['breadcrumbs'][] = ['label' => 'Emily Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emily-order-view">

    <h1>Продукт № <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->idProduct], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->idProduct], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a('Список товарів', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idProduct',
            'nameProduct',
            'priceProduct',
            //'type_idType',
            [
                'attribute' => 'type_idType',
                'value' =>  $model->typeIdType->nameType,
                
            ],
            'description:html',
            
            [
                'attribute' => 'namePhoto',
                
                'value' => Html::img("@web/images/portfolio/{$model->namePhoto}", ['alt' => $model->namePhoto, 'width' => '90px', 'title' => $model->namePhoto]),
           
       
                 'format' => 'html',
             ],
        ],
    ]) ?>

</div>
