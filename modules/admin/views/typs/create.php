<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\EmilyType */

$this->title = 'Створити новий тип';
$this->params['breadcrumbs'][] = ['label' => 'Emily Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emily-order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
