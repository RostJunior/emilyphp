<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\EmilyType */

$this->title = 'Update Emily Type: ' . $model->idType;
$this->params['breadcrumbs'][] = ['label' => 'Emily Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idType, 'url' => ['view', 'id' => $model->idType]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="emily-type-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
