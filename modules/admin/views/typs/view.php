<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\models\EmilyType */

$this->title = $model->idType;
$this->params['breadcrumbs'][] = ['label' => 'Emily Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emily-order-view">

    <h1>Тип № <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редагувати', ['update', 'id' => $model->idType], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Видалити', ['delete', 'id' => $model->idType], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Список типів', ['index'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idType',
            'nameType',
        ],
    ]) ?>

</div>
