<?php use yii\helpers\Html;?>
<?php if(!empty($session['cart'])): ?>

    <div class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Фото</th>
                    <th>Назва</th>
                    <th>Кількість</th>
                    <th>Ціна</th>
                    <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($session['cart'] as $id=>$item):?>
                 <tr>  
                     <td><?=
                     Html::img("@web/images/portfolio/{$item['img']}", ['alt' => "{$item['name']}", 'class' => 'cart-modal-img', 'width' => '50px']);
                     ?></td>
                     <td><?= $item['name']?></td>
                      <td><?= $item['qty']?></td>
                       <td><?= $item['price']?></td>
                       <td><span data-id="<?= $id?>" class="del-item glyphicon glyphicon-remove text-danger" aria-hidden="true"></span></td>
               
                  </tr> 
                <?php endforeach;?>
                  <tr>
                      <td colspan="4">Итого:</td>
                      <td><?= $session['cart.qty']?> шт.</td>
                  </tr>
                   <tr>
                      <td colspan="4">На сумму:</td>
                      <td><?= $session['cart.sum']?> грн.</td>
                  </tr>
            </tbody>
            
        </table>
</div>

<?php else: ?>
<h3>Кошик порожній!</h3>
<?php endif; ?>


