<?php


$res = Array(
        "apiKey" => "505c0b389f71f705bd081d753e9378b7",
        "modelName" => "InternetDocument",
        "calledMethod" => "save",
        "methodProperties" => [
         "NewAddress" => "1",
        "PayerType" => "Recipient",
        "PaymentMethod" => "Cash",
        "CargoType" => "Cargo",
        "Weight" => "0.5",
        "ServiceType" => "WarehouseWarehouse",
        "SeatsAmount"=> "1",
        "Description"=> "Інше",
        "Cost"=> "$summ",
        "CitySender"=> "db5c88f5-391c-11dd-90d9-001a92567626",
        "Sender"=> "630b61e9-fe4d-11e6-8ba8-005056881c6b",
        "SenderAddress"=> "16922806-e1c2-11e3-8c4a-0050568002cf",
        "ContactSender"=> "ebdaa999-010b-11e7-8ba8-005056881c6b",
        "SendersPhone"=> "380991234567",
        "RecipientCityName"=> "$cityName",
        "RecipientArea"=> "",
        "RecipientAreaRegions"=> "",
        "RecipientAddressName"=> "$address",
        "RecipientHouse"=> "",
        "RecipientFlat"=> "",
        "RecipientName"=> "$nameUser",
        "RecipientType"=> "PrivatePerson",
        "RecipientsPhone"=> "$phone"
        ],
);


/*

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'http://testapi.novaposhta.ua/v2.0/json/Address/getCities');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($res));
		curl_setopt($ch, CURLOPT_POST, 1);
		
		$response = curl_exec($ch);
		curl_close($ch);
                
                $result = is_array($response)
				? $response
				: json_decode($response, 1);
		
	
                
                Cities::deleteAll();
                
                $n = 1;	
                foreach ($result['data'] as $key=> $val) {
                           
			 $cities = new Cities();
                         $cities->id = $n;
                         $cities->description = $val['Description'];
                         $cities->descriptionRu = $val['DescriptionRu'];
                         $cities->ref = $val['Ref'];
                         $cities->area = $val['Area'];
                         $cities->cityID = $val['CityID'];
                         $cities->save();
                         ++$n;
		};
         
        Yii::$app->getResponse()->redirect('/admin');
 * 
 * 
 */