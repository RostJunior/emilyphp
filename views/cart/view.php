<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;


?>

<!-- navbar -->
	<nav class="navbar navbar-inverse" role="navigation">
        <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#NavCol">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <a class="navbar-brand bg-primary" href="<?= \yii\helpers\Url::to(['/site/login'])?>">ВХІД</a>
        </div>
        <?php if(!Yii::$app->user->isGuest):?>
        <div class="logout">
            	<a  href="<?= \yii\helpers\Url::to(['/site/logout'])?>"><i class="fa fa-user"></i>
	<?= Yii::$app->user->identity['nameUsers']?> Выход</a><br/>
         </div>
            <?php endif;?>
        <?php if(!Yii::$app->user->isGuest):?>
            <div class="enter-admin">
                <a class="navbar-brand bg-primary"  href="<?= \yii\helpers\Url::to(['/admin'])?>" title="Вхід">Адмінка</i></a>
                
            </div>
            <?php endif;?>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="NavCol1">
         <ul class="nav navbar-nav1 navbar-right">
           
            <li><a href="<?= \yii\helpers\Url::to(['/'])?>">Головна</a></li>
            <li><a href="<?= \yii\helpers\Url::to(['/#work'])?>" >Мої вироби</a></li>
            
          </ul>
        <div class="cart">
                <a href="/cart/show" class="cart-img" title="Кошик" onclick="return getCart()"><img src="/images/basket.gif"></a>
       
            </div>
         
        </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

<div class="page222 main clearfix page">
   
<div class="container">
    <div>
         <?php if( Yii::$app->session->hasFlash('success_order') ): //вывод сообщения успешного сохранения товара?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('success_order'); ?>
        </div>
    <?php endif;?>

    <?php if( Yii::$app->session->hasFlash('error_order') ): //вывод в случае ошибки сохранения заказа?>
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo Yii::$app->session->getFlash('error_order'); ?>
        </div>
    <?php endif;?>
    </div>
    
    <?php if(!empty($session['cart'])): ?>

    <div id="cart-order" class="table-responsive">
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>Фото</th>
                    <th>Назва</th>
                    <th>Кількість</th>
                    <th>Ціна</th>
                    <th>Сумма</th>
                    <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($session['cart'] as $id=>$item):?>
                 <tr>  
                     <td><?=
                     Html::img("@web/images/portfolio/{$item['img']}", ['alt' => "{$item['name']}", 'class' => 'cart-modal-img', 'width' => '50px']);
                     ?></td>
                     <td><a href="<?= Url::to(['product/info', 'id'=>$id])?>"><?= $item['name']?></a></td>
                      <td><?= $item['qty']?></td>
                       <td><?= $item['price']?></td>
                        <td><?= $item['price'] * $item['qty']?></td>
                        <td><a href="<?= Url::to(['cart/del-item-order', 'id'=>$id])?>"><span class="del-item glyphicon glyphicon-remove text-danger" aria-hidden="true"></span></a></td>
               
                  </tr> 
                <?php endforeach;?>
                  <tr>
                      <td colspan="4">Итого:</td>
                      <td><?= $session['cart.qty']?> шт.</td>
                      <td></td>
                  </tr>
                   <tr>
                      <td colspan="4">На сумму:</td>
                      <td><?= $session['cart.sum']?> грн.</td>
                       <td></td>
                  </tr>
            </tbody>
            
        </table>
</div>
    
    <hr />
  <h2>Виберіть спосіб доставки:</h2>
    <div type="submit" id="smv" onclick="showSm()" class="btn btn-success">Самовивіз</div>
    <div type="submit" id="np"  onclick="showNp()" class="btn btn-success">Нова Пошта</div>
   
    <div id="deliv" style="display:none">   
    <?php $form = ActiveForm::begin(['id' => 'samov'])?>
       <h2>Для оформлення замовлення, заповніть всі поля:</h2> 
    <?= $form->field($order, 'nameUser')->textInput(['id' => 'nameSam'])?>
    <?= $form->field($order, 'emailUser')->input('email')->textInput(['id' => 'emailSam'])?>
    <?= $form->field($order, 'phoneUser')->widget(\yii\widgets\MaskedInput::className(), [
    'name' => 'phone',
    'mask' => '389999999999',
])?>
    
     <hr />
   
    
        <h2>Вид доставки: самовивіз</h2>
        <?= Html::submitButton('Замовити', ['class' => 'btn btn-success', 'formaction' => '/cart/view'])?>
        <?php ActiveForm::end()?>
    </div>
        
     <div id="nevpost" style="display:none">
          <?php $formn = ActiveForm::begin(['id' => 'nevpost'])?>
         
         <h5>Для отримання замовлення введіть П.І.Б. контактної особи (повністю)</h5>
         
          <?= $formn->field($dostavka, 'contactUser')?>
         <?= $formn->field($order, 'emailUser')->input('email')?>
    <?= $formn->field($dostavka, 'phoneUser')->widget(\yii\widgets\MaskedInput::className(), [
         'name' => 'phoneNP',
        'mask' => '380999999999',
])?>
         <h5>Виберіть місто і відділення</h5>
    
    <?= $formn->field($cities, 'description')->dropDownList(
 \yii\helpers\ArrayHelper::map(app\models\Cities::find()->all(), 'cityID', 'description'), 
            
            ['prompt'=>'Виберіть місто', 'onchange' => 'getCont()', 'id'=>'getcities']
    )?>
   
        
       <?= $formn->field($office, 'description')->dropDownList(
                ['0'=>'Виберіть відділення']
               )?>  
    
     <?= Html::submitButton('Замовити', ['class' => 'btn btn-success', 'formaction' => '/cart/ordernp'])?>
   
   
    </div>   
        
      
    <?php ActiveForm::end()?>
    
    </div>
    
<?php else: ?>
<h3>Кошик порожній!</h3>
<?php endif; ?>
    

</div>
 


</div>
 

<div id="contact" class="page page-bgcolor">
		<div class="container">
		<div class="row">
          <div class="col-md-10  col-md-offset-1">
		    <div class="build title-page">
				<h2 class="text-center">Контакти</h2>	
				<div class="line-title bg-primary"></div>
			</div>
		   </div><!-- end col -->
		</div><!-- end row -->
	  	<div class="row">
			<div class="col-md-4 ">
				<div class="build contact clearfix text-center">
					<span class="fa fa-map-marker"></span>
						<p>вул. Шевченка<br />м. Львів, Україна<p>
				</div>			
			</div><!-- end col -->
			<div class="col-md-4 ">
				<div class="build contact clearfix text-center">
					<span class="fa fa-phone"></span>
					
					<p>mob.<a href="tel:+3 8 097 738 46 52">+380977384652</a></p>
					
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="build contact clearfix text-center">
					<span class="fa fa-envelope"></span>
					<p>ros-gap601@yandex.ru</p>
					<p>liliya@emily.hol.es</p>
				</div>
			</div>
		</div><!-- end row -->
	
	  </div><!-- end container -->
	</div>