

    <div class="main-nav">
	  <!-- navbar -->
	<nav class="navbar navbar-inverse" role="navigation">
        <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#NavCol">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand bg-primary" href="<?= \yii\helpers\Url::to(['/site/login'])?>">ВХІД</a>
        </div>
          
       
               <?php if(!Yii::$app->user->isGuest):?>
        <div class="logout">
            	<a  href="<?= \yii\helpers\Url::to(['/site/logout'])?>"><i class="fa fa-user"></i>
	<?= Yii::$app->user->identity['nameUsers']?> Выход</a><br/>
         </div>
<?php endif;?>
          <?php if(!Yii::$app->user->isGuest):?>
            <div class="enter-admin">
                <a class="navbar-brand bg-primary"  href="<?= \yii\helpers\Url::to(['/admin'])?>" title="Вхід">Адмінка</i></a>
                
            </div>
            <?php endif;?>
            <div class="blog">
            <a class="navbar-brand bg-primary" href="<?= \yii\helpers\Url::to(['/site/posts'])?>">Блог про біжутерії</a>
            </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="NavCol">
            
         <ul class="nav navbar-nav navbar-right">
           
            <li class="current"><a href="#about" class="linear">Головна</a></li>
            <li><a href="#aboutMore" class="linear">Інфо</a></li>
            <li><a href="#work" class="linear">Мої вироби</a></li>
            <li><a href="#contact" class="linear">Контакти</a></li>
         </ul>
            <div class="cart">
                <a href="<?= \yii\helpers\Url::to(['/cart/show'])?>" class="cart-img" title="Кошик" onclick="return getCart()"><img src="/images/basket.gif"></a>
       
            </div >
            
        </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
	  
	</div>

<!-- Full Page Image Header Area -->
    <div id="about" class="header">

	<div class="maskHeader"></div>
	<div class="main-caption">
		<div class="logo">
			<img src="/images/jk.png" alt="logo" />
		</div>
			<!-- slider -->
	<div id="flex-head" class="flexslider">
		<ul class="slides">
			<li>
				<h1>Прикраси для волосся в стилі канзаші</h1>	
				
			</li>
			<li>
				<h1>Сайт працює в тестовому режимі</h1>	
				
			</li>
			
		</ul>
	</div> 
    <!-- end slider --> 
		<a href="#aboutMore" class="btn btnAbout btn-clear border-color color-primary btn-lg linear">ДЕТАЛЬНІШЕ</a>
	</div><!--  end main caption -->
			
    </div>
	<!-- end header -->
    <!-- /Full Page Image Header Area -->

<div id="aboutMore" class="page">
      <div class="container">
		<div class="row">
          <div class="col-md-10  col-md-offset-1">
		    <div class="build title-page">
				<h2 class="text-center">Доброго дня!</h2>	
				<div class="line-title bg-primary"></div>
			</div>
		   </div><!-- end col -->
		</div><!-- end row -->
		
        <div class="row">
           <div class="my-info">
			   <div class="build main-about">
				   <div class="row">
				       
				       <div class="col-md-9">
				            <div class="about-content">
						<p> Мене звати Лілія. Виготовляю канзаші-прикраси з квітами з атласних стрічок та тканини. 
						Створю для Вас красиву неповторну прикрасу.
						Можлива робота за Вашими ескізами, пропонуйте свої варіанти, або з мотивів моїх робіт.
						Із задоволенням виконаю щось незвичайне і нове, з стрічок в техніці Канзаші, і Ви будете неповторні.
						Обручі, резиночки, шпильки, брошки, стріли, краби, пов’язки – замовлення може бути на цих основах.
						Вартість залежить від складності роботи. Звертайтесь по телефону або <a href="http://vk.com/id289852074" target="_blank" title="Лилия Канзаши">Вконтакте</a></p>
						<a href="#contact" class="btn btn-clear btn-lg border-color linear hire">Контакти</a>
					</div>
				       </div>
				   </div>
				  
					
				</div>
            </div>
			<!-- end col -->
		 
       
			
        </div>
		
		<!-- end row -->
      </div>
      </div>
    
    <!-- /Intro -->   

    <!-- Portfolio -->
    <div id="work" class="page clearfix">
      <div class="container">
		<div class="row">
          <div class="col-md-10  col-md-offset-1">
		    <div class="build title-page">
				<h2 class="text-center">Мої Вироби</h2>	
				<div class="line-title bg-primary"></div>
			</div>
		   </div><!-- end col -->
		</div><!-- end row -->
		   <div class="col-md-12">
                
                <ul id="filterOptions" class="clearfix">
                    <li><a class="btn btn-link linear" href="#" data-group="all">Всі</a></li>
                    
                    <?php foreach ($typs as $typ):?>
                    <li><span>/</span></li>
                    <li><a class="btn btn-link linear" href="#" data-group="<?= $typ->nameType?>"><?= $typ->nameType?></a></li>
					
                                        
                     <?php endforeach;?>                   
                    
                </ul>
               
            </div><!--end-col-->
            
            
		  <div class="col-md-12">
		  <div class="folio-content clearfix">
		   <div class="row">
		   <div class="container_folio clearfix gallery" id="grid">
                       <?php foreach ($products as $prod):?>
                       <div class="col-xs-12 col-sm-4 col-md-4 box" data-groups='["all", "<?= $prod->typs->nameType?>"]'>
			<div class="folio-img">
				<div class="portfolio-item ">
					<div class="thumbnail">
					   <div class="thumb-img">
					  
					  <div class="link-attr">
                                              <a href="images/portfolio/<?=$prod->namePhoto;?>" data-gallery="global-gallery" data-parent="" data-toggle="lightbox" data-title="<?= $prod->nameProduct?>" class="link-search animated linear" title="Збільшити"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
                                              <a href="<?= \yii\helpers\Url::to(['product/info',
                                                                'id' => $prod->idProduct])?>" class="link-chain animated linear" title="Докладніше"><i class="fa fa-info-circle" aria-hidden="true"></i></a>
                                              
                                          </div>
                                                <?= yii\helpers\Html::img("@web/images/portfolio/{$prod->namePhoto}", ['alt' => "$prod->namePhoto", 'class' => 'linear img-portfolio img-responsive']) ?>
						
					  
					   
					   <div class="folio-caption">
					   <i class="fa fa-caret-up"></i>
                                           <p class="to-cart"> Ціна: <?= $prod->priceProduct?> грн.  <a href="<?= yii\helpers\Url::to(['/cart/add', 'id' => $prod->idProduct])?>" class="add-to-cart" data-id ="<?= $prod->idProduct?>" title="В кошик"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a></p>
                                                
                                            
                                            
					   </div>
					   </div>
					</div>
				
				</div>
			</div>
                          
			</div>
			
			<?php endforeach;?>
		   </div>
		  
		   </div>
		  </div><!-- end folio content -->
		  </div><!-- end col -->
      </div><!--end container-->
	 
	 
    </div>
    <!-- /Portfolio -->
    
    <!--contact-->
    <div id="contact" class="page page-bgcolor">
		<div class="container">
		<div class="row">
          <div class="col-md-10  col-md-offset-1">
		    <div class="build title-page">
				<h2 class="text-center">Контакти</h2>	
				<div class="line-title bg-primary"></div>
			</div>
		   </div><!-- end col -->
		</div><!-- end row -->
	  	<div class="row">
			<div class="col-md-4 ">
				<div class="build contact clearfix text-center">
					<span class="fa fa-map-marker"></span>
						<p>вул. Шевченка<br />м. Львів, Україна<p>
				</div>			
			</div><!-- end col -->
			<div class="col-md-4 ">
				<div class="build contact clearfix text-center">
					<span class="fa fa-phone"></span>
					
					<p>mob.<a href="tel:+3 8 097 738 46 52">+380977384652</a></p>
					
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="build contact clearfix text-center">
					<span class="fa fa-envelope"></span>
					<p>ros-gap601@yandex.ru</p>
					<p>liliya@emily.hol.es</p>
				</div>
			</div>
		</div><!-- end row -->
	
	  </div><!-- end container -->
	</div>