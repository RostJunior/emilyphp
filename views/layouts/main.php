<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    
      <?php
        $this->registerJsFile('js/html5shiv.js', 
            ['position' => \yii\web\View::POS_HEAD, 'condition' => 'lte IE9']);
    
    ?>  
    <?php
        $this->registerJsFile('js/respond.min.js', 
            ['position' => \yii\web\View::POS_HEAD, 'condition' => 'lte IE9']);
    
    ?> 

  
	<link rel="icon" href="/images/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/images//favicon.ico" type="image/x-icon">
	



</head>





<body>
    
    
<?php $this->beginBody() ?>
  <div id="boxWrapp">
    

        <?= $content ?>
    
   </div>  
           
    
    <!-- Footer -->
    <footer class="bg-black">
      <div class="container">
        <div class="row">
			<div class="col-md-6 ">
				<div class="cp-right">
					<p>&copy; 2015 <a href="#" class="color-primary linear">@Rost@</a>.Reserved. </p>
				</div><!-- end build -->
			</div><!-- end col -->
			
			<div class="col-md-6 text-right">
		
			<ul class="list-inline">
			<li><a href="http://vk.com/id289852074" class="socIcon color-primary linear"><i class="fa fa-vk fa-2x"></i></a></li>
			<li><a href="#" class="socIcon color-primary linear"><i class="fa fa-twitter fa-2x"></i></a></li>
			<li><a href="#" class="socIcon color-primary linear"><i class="fa fa-dribbble fa-2x"></i></a></li>
			</ul>
		
			</div>
        </div>
      </div>
    </footer>

    
    <!-- /Footer -->
 <?php $this->endBody() ?>
<?php>
    //Cart (modal)
yii\bootstrap\Modal::begin([
    'header' => '<h2>Кошик</h2>',
    'id' => 'cart',
    'size' => 'modal-lg',
    'footer' => 
    '<button type="button" class="btn btn-default" data-dismiss="modal">Продовжити</button>
         <a href="'.yii\helpers\Url::to(["/cart/view"]).'" class="btn btn-success">Оформити замовлення</a> 
        <button type="button" class="btn btn-danger" onclick="clearCart()" >Очистити кошик</button>'
]);
yii\bootstrap\Modal::end();

?>


 

</body>
</html>
<?php $this->endPage() ?>
