<?php
use \app\models\Cities;

$res = Array(
	"modelName" => "Address",
	"calledMethod" => "getCities",
	"methodProperties" => Array(""=> ""),
	"apiKey"=> "505c0b389f71f705bd081d753e9378b7"
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'http://testapi.novaposhta.ua/v2.0/json/Address/getCities');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($res));
		curl_setopt($ch, CURLOPT_POST, 1);
		
		$response = curl_exec($ch);
		curl_close($ch);
                
                $result = is_array($response)
				? $response
				: json_decode($response, 1);
		
	
                
                Cities::deleteAll();
                
                $n = 1;	
                foreach ($result['data'] as $key=> $val) {
                           
			 $cities = new Cities();
                         $cities->id = $n;
                         $cities->description = $val['Description'];
                         $cities->descriptionRu = $val['DescriptionRu'];
                         $cities->ref = $val['Ref'];
                         $cities->area = $val['Area'];
                         $cities->cityID = $val['CityID'];
                         $cities->save();
                         ++$n;
		}
         
        Yii::$app->getResponse()->redirect('/admin');