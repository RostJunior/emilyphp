<?php
 use \app\models\EmilyPosts;


            EmilyPosts::deleteAll();
            EmilyPosts::autoIncr();
            $posts = EmilyPosts::getPage();
            $news = EmilyPosts::getNews($posts);
           if(!EmilyPosts::setPosts($news)) {
               $message = "Помилка зберігання постів в базу";
                return $this->render('error', compact('message'));
            }
           
          $pagination = EmilyPosts::getPagination($posts);
           
              foreach ($pagination as $page) {
                    $postNext = EmilyPosts::getPageUrl($page);
                    $newsNext = EmilyPosts::getNews($postNext);
                    if(!EmilyPosts::setPosts($newsNext)) {
                        $message = "Помилка зберігання постів в базу";
                        return $this->render('error', compact('message'));
                        }
                }
         
                Yii::$app->getResponse()->redirect('/admin');
           
