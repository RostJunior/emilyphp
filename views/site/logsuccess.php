<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page222">
    <!-- navbar -->
	<nav class="navbar navbar-inverse" role="navigation">
        <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#NavCol">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
         <a class="navbar-brand bg-primary" href="<?= \yii\helpers\Url::to(['/site/login'])?>">ВХІД</a>
        </div>
        <?php if(!Yii::$app->user->isGuest):?>
        <div class="logout">
            	<a  href="<?= \yii\helpers\Url::to(['/site/logout'])?>"><i class="fa fa-user"></i>
	<?= Yii::$app->user->identity['nameUsers']?> Выход</a><br/>
         </div>
            <?php endif;?>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="NavCol1">
         <ul class="nav navbar-nav1 navbar-right">
           
            <li><a href="<?= \yii\helpers\Url::to(['/'])?>">Головна</a></li>
            <li><a href="<?= \yii\helpers\Url::to(['/#work'])?>" >Мої вироби</a></li>
            
          </ul>
        <div class="cart">
                <a href="/cart/show" class="cart-img" title="Кошик" onclick="return getCart()"><img src="/images/basket.gif"></a>
       
            </div>
         
        </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>

    
    <div class="container">
<div class="site-login container">
    <div class="admin-navigation">
     <?php if( Yii::$app->session->hasFlash('succes_reg') ): ?>
         <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
           <?php echo Yii::$app->session->getFlash('succes_reg'); ?>
        </div>
    <?php endif;?>
   
        <div class="form-group">
            <div class="col-lg-offset-1 col-lg-11">
              <p> <?= Html::a('Вхід', ['login'], ['class' => 'btn btn-primary login-button', 'name' => 'login-button']) ?></p>
            </div>
        </div>
    </div>
</div>
    </div>  
</div>
<div id="contact" class="page page-bgcolor">
		<div class="container">
		<div class="row">
          <div class="col-md-10  col-md-offset-1">
		    <div class="build title-page">
				<h2 class="text-center">Контакти</h2>	
				<div class="line-title bg-primary"></div>
			</div>
		   </div><!-- end col -->
		</div><!-- end row -->
	  	<div class="row">
			<div class="col-md-4 ">
				<div class="build contact clearfix text-center">
					<span class="fa fa-map-marker"></span>
						<p>вул. Шевченка<br />м. Львів, Україна<p>
				</div>			
			</div><!-- end col -->
			<div class="col-md-4 ">
				<div class="build contact clearfix text-center">
					<span class="fa fa-phone"></span>
					
					<p>mob.<a href="tel:+3 8 097 738 46 52">+380977384652</a></p>
					
				</div>
			</div>
			<div class="col-md-4 ">
				<div class="build contact clearfix text-center">
					<span class="fa fa-envelope"></span>
					<p>ros-gap601@yandex.ru</p>
					<p>liliya@emily.hol.es</p>
				</div>
			</div>
		</div><!-- end row -->
	
	  </div><!-- end container -->
	</div>


    