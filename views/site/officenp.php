<?php
use app\models\EmilyOfficeNP;


$res = Array(
	 "modelName" => "AddressGeneral",
         "calledMethod" => "getWarehouses",
         "methodProperties" => ["" => ""],
          "apiKey" => "505c0b389f71f705bd081d753e9378b7"
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'http://testapi.novaposhta.ua/v2.0/json/AddressGeneral/getWarehouses');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($res));
		curl_setopt($ch, CURLOPT_POST, 1);
		
		$response = curl_exec($ch);
		curl_close($ch);
                
                $result = is_array($response)
				? $response
				: json_decode($response, 1);
		
                EmilyOfficeNP::deleteAll();
                $n=1;
               
                foreach ($result['data'] as $key => $val) {
                    if($val['TypeOfWarehouse'] == "f9316480-5f2d-425d-bc2c-ac7cd29decf0" || $val['TypeOfWarehouse'] == "95dc212d-479c-4ffb-a8ab-8c1b9073d0bc" ) {continue;}
                    $office = new EmilyOfficeNP();
                    $office->id = $n;
                    $office->siteKey = $val['SiteKey'];
                    $office->description = $val['Description'];
                    $office->descriptionRu = $val['DescriptionRu'];
                    $office->typeOfWarehouse = $val['TypeOfWarehouse'];
                    $office->refOffice = $val['Ref'];
                    $office->cityRef = $val['CityRef'];
                    $office->cityDescription = $val['CityDescription'];
                    $office->cityDescriptionRu = $val['CityDescriptionRu'];
                    $office->number = $val['Number'];
                    $office->save();
                    ++$n;
                }
            
            Yii::$app->getResponse()->redirect('/admin');